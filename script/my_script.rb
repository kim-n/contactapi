require 'addressable/uri'
require 'rest-client'

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users.html'
).to_s

curl = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/contacts.html'
).to_s

csurl = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/contact_shares.html'
).to_s

# puts RestClient.get(url)

# puts RestClient.post(url, {user: {name: "John", email: "John@smith.com"}})
# puts RestClient.post(url, {user: {name: "Brian", email: "brian@stearns.com"}})
# puts RestClient.post(url, {user: {name: "Kimberly", email: "kimberly@narine.com"}})
#
# puts RestClient.post(curl, {contact: {name: "George", email: "George@george.com", user_id: 1}})
# puts RestClient.post(curl, {contact: {name: "Mike", email: "Mike@Mike.com", user_id: 2}})
# puts RestClient.post(curl, {contact: {name: "Kate", email: "kate@Kate.com", user_id: 2}})

puts RestClient.post(url, {user: {name: "Steve", email: "Steve@ben.com"}})

# puts RestClient.post(csurl, {contact_share: {user_id: 4, contact_id: 1}})
# puts RestClient.post(csurl, {contact_share: {user_id: 4, contact_id: 1}})
#