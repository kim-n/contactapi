class UsersController < ApplicationController

  def index
    @users = User.all

    render :json => @users
    # render :text =>"I'm in the index action!"
  end

  def create
    # render :text => "TEXT"
    user = User.new(params[:user])
    if user.save
      render :json => user
    else
      render :json => user.errors, :status => :unprocessable_entity
    end
  end

  def show
    render :text =>"Show! #{params[:id]}"
  end

  def favorite_contacts
    @fav_contacts = Contact.favorite_contacts_for_user(params[:user_id-1])
    render :json => @fav_contacts
  end
end
