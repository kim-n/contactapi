class ContactsController < ApplicationController

  def index
    # render :json => params[:user_id]
    @contacts = Contact.contacts_for_user(params[:user_id-1])
    render :json => @contacts
    # render :text =>"I'm in the index action!"
  end

  def create
    # render :text => "TEXT"
    contact = Contact.new(params[:contact])
    if contact.save
      render :json => contact
    else
      render :json => contact.errors, :status => :unprocessable_entity
    end
  end

  def show
    contact = Contact.find_by_id(params[:id])
    render :json => contact
  end

  def destroy
    Contact.find_by_id(params[:id]).destroy
    head :ok
  end

  def update
    contact = Contact.new(params[:contact])
    if contact.update_attributes(params[:contact])
      render :json => contact
    else
      render :json => contact.errors, :status => :unprocessable_entity
    end
  end

end
