class ContactSharesController < ApplicationController


  def create
    contact_share = ContactShare.new(params[:contact_share])
    if contact_share.save
      render :json => contact_share
    else
      render :json => contact_share.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    ContactShare.find_by_id(params[:id]).destroy
    head :ok
  end


  # def index
  #   @contact_shares = ContactShare.all
  #
  #   render :json => @contact_shares
  #   # render :text =>"I'm in the index action!"
  # end
  #
  # def show
  #   contact_share = ContactShare.find_by_id(params[:id])
  #   render :json => contact_share
  # end
  #
  # def update
  #   contact_share = ContactShare.new(params[:contact_share])
  #   if contact.update_attributes(params[:contact_share])
  #     render :json => contact_share
  #   else
  #     render :json => contact_share.errors, :status => :unprocessable_entity
  #   end
  # end

end
