class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :user_id
  belongs_to(:user,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id)

  has_many(:contact_shares,
  class_name: "ContactShare",
  foreign_key: :contact_id,
  primary_key: :id)

  def self.contacts_for_user(user_id)
    contacts_query = <<-SQL
    SELECT *
    FROM
    contacts
    LEFT OUTER JOIN
    contact_shares
    ON
    contacts.id = contact_shares.contact_id
    WHERE
    contacts.user_id = #{user_id} OR
    contact_shares.user_id = #{user_id}

    SQL
    Contact.find_by_sql(contacts_query)
  end

  def self.favorite_contacts_for_user(user_id)
    contacts_query = <<-SQL
    SELECT *
    FROM
    contacts
    WHERE
    contacts.user_id = #{user_id} AND contacts.favorite = 't'
    SQL
    Contact.find_by_sql(contacts_query)
  end

end
