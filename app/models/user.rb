class User < ActiveRecord::Base
   attr_accessible :email, :name

   has_many(:contacts,
   class_name: "Contact",
   foreign_key: :user_id,
   primary_key: :id)

   has_many(:contact_shares,
   class_name: "ContactShare",
   foreign_key: :user_id,
   primary_key: :id)

   has_many :shared_contacts, through: :contact_share, source: :contact
   # has_many :contacts_being_shared, through: :contact_share, source: :user

end
