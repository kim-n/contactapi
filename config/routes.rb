RoutesAndControllers::Application.routes.draw do
  resources :users do
    resources :contacts, :only => [:index]
    member do
      get 'favorite_contacts'
    end
  end
  resources :contacts, :only => [:create, :show, :update, :destroy]
  resources :contact_shares, :only => [:create, :destroy]

  # get    'users'   =>       'users#index'
  # post   'users '  =>       'users#create'
  # get   'users/:id'   =>   'users#show'
  # put    'users/:id'  =>    'users#update'
  # delete 'users/:id'  =>   'users#destroy'

  # get 'users/new'  =>   'users#new'
  # get 'users/:id/edit' => 'users#edit'
end
