class AddGiverToContactShare < ActiveRecord::Migration
  def change
    add_column :contact_shares, :giver_id, :integer
  end
end
